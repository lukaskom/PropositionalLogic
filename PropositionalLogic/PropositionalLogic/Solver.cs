﻿using PropositionalLogic.Code;
using PropositionalLogic.Formula;
using PropositionalLogic.Operator;
using PropositionalLogic.Operator.Algebraic;
using PropositionalLogic.ParserLogic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PropositionalLogic
{
    public class Solver
    {
        public Configuration Config { get; set; }

        public Solver()
            : this(new Configuration()) {}
        public Solver(Configuration config)
            => Config = config ?? throw new ArgumentNullException("Configuration in Solver must not be null.");

        public FormulaNode Solve(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return null;

            var parser = new Parser(Config);
            var parsedItems = parser.Parse(str);

            var root = SolveRec(parsedItems);

            return root;
        }

        private FormulaNode SolveRec(IList<ParsedItem> items)
        {
            if (!items.Any())
                return null;

            var lowestPriority = items.Min(t => t.Priority);

            var beforeHighestNode = items.TakeWhile(t => t.Priority != lowestPriority).ToList();
            var parsedItem = items[beforeHighestNode.Count];
            var highestNode = parsedItem.ToNode(Config);
            var afterHighestNode = items.Skip(beforeHighestNode.Count + 1).ToList();

            if (parsedItem is ParsedParentheses parent)
            {
                if (beforeHighestNode.Any())
                    throw new SolverException("Ilegal parsed item(s) before parentheses.");

                if (afterHighestNode.Any())
                    throw new SolverException("Ilegal parsed item(s) after parentheses.");

                var par = (highestNode as Parentheses);
                par.ChildNode = SolveRec(parent.ParsedContent);

                return par;
            }

            if ((highestNode is Minus minus) && !beforeHighestNode.Any())
                highestNode = new UnaryMinus(Config);
            
            if (highestNode is BinaryOperator binary)
            {
                if (!beforeHighestNode.Any())
                    throw new SolverException($"Missing parsed item(s) before binary operator: {binary.GetType().Name}");

                if (!afterHighestNode.Any())
                    throw new SolverException($"Missing parsed item(s) after binary operator: {binary.GetType().Name}");

                binary.LeftChildNode = SolveRec(beforeHighestNode);
                binary.RightChildNode = SolveRec(afterHighestNode);
                return binary;
            }

            if (highestNode is UnaryOperator unary)
            {
                if (beforeHighestNode.Any())
                    throw new SolverException($"Ilegal parsed item(s) before unary operator: {unary.GetType().Name}");

                if (!afterHighestNode.Any())
                    throw new SolverException($"Missing parsed item(s) after unary operator: {unary.GetType().Name}");

                unary.ChildNode = SolveRec(afterHighestNode);
                return unary;
            }

            if (highestNode is AtomicFormula atom)
            {
                if (beforeHighestNode.Any())
                    throw new SolverException($"Ilegal parsed item(s) before atomic formula: {atom.GetType().Name}");

                if (afterHighestNode.Any())
                    throw new SolverException($"Ilegal parsed item(s) after atomic formula: {atom.GetType().Name}");

                return atom;
            }

            throw new SolverException("Could not recognize parsed item.");
        }
    }
}
