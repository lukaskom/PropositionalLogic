﻿using PropositionalLogic.Code;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PropositionalLogic.ParserLogic
{
    public class Parser
    {
        public Configuration Config { get; set; }

        public Parser(Configuration config)
            => Config = config ?? throw new ArgumentNullException("Configuration in Solver must not be null.");

        public List<ParsedItem> Parse (string formula)
        {
            var ret = new List<ParsedItem>();
            if (string.IsNullOrWhiteSpace(formula))
                return ret;

            var splitPars = SplitByParentheses(formula);
            var separators = Config.GetAllSeparators;

            foreach ((var str, var par) in splitPars)
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    var list = str.IndicesOfAny(separators);
                    var pos = 0;
                    foreach ((var occurence, var item) in list)
                    {
                        var strList = str
                            .Substring(pos, occurence - pos)
                            .Split(new char[0]) // whitespace split
                            .Where(t => !string.IsNullOrEmpty(t))
                            .ToList();
                        ret.AddRange(strList.Select(t => new Unknown(t)));
                        
                        var key = str.Substring(occurence, item.Length);
                        if (StringToItem.TryGetValue(key, out var tmp))
                            ret.Add(tmp);
                        else
                            ret.Add(new Unknown(key));

                        pos = occurence + item.Length;
                    }
                    if (pos < str.Length)
                    {
                        var strList = str
                            .Substring(pos)
                            .Split(new char[0]) // whitespace split
                            .Where(t => !string.IsNullOrEmpty(t))
                            .ToList();
                        ret.AddRange(strList.Select(t => new Unknown(t)));
                    }
                }
                if (par != null)
                {
                    par.ParsedContent = Parse(par.StringContent);
                    ret.Add(par);
                }
            }

            return ret;
        }

        public List<(string str, ParsedParentheses par)> SplitByParentheses(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return new List<(string str, ParsedParentheses par)>();

            var open = str.IndexOf(Config.OpenParenthesisSign);
            var closed = str.IndexOf(Config.ClosedParenthesisSign);

            if (closed < 0 && open < 0)
                return new List<(string str, ParsedParentheses par)>() { (str, null) };

            if (closed < open)
            {
                if (closed < 0)
                    throw new ParserException("There is no closing parentheses for an opening one.", open);
                else
                    throw new ParserException("Closing parentheses before an opening one.", closed);
            }

            var ret = new List<(string str, ParsedParentheses par)>();
            var inceptionCnt = 1;
            var position = open + 1;

            while(inceptionCnt != 0)
            {
                var nextOpen = str.IndexOf(Config.OpenParenthesisSign, position);
                closed = str.IndexOf(Config.ClosedParenthesisSign, position);

                if (nextOpen < 0 && closed < 0)
                    throw new ParserException("There is no closing parentheses for an opening one.", open);

                if (nextOpen < 0 || closed < nextOpen)
                {
                    inceptionCnt--;
                    position = closed + 1;
                }
                else if (closed < 0 || nextOpen < closed)
                {
                    inceptionCnt++;
                    position = nextOpen + 1;
                }
            }
            ret.Add((str.Substring(0, open), new ParsedParentheses(str.Substring(open + 1, closed - 1))));

            if (closed + 1 < str.Length)
                ret.AddRange(SplitByParentheses(str.Substring(closed + 1)));

            return ret;
        }

        private Dictionary<string, ParsedItem> StringToItem
            => new Dictionary<string, ParsedItem>()
            {
                { Config.TimesSign, new ParsedTimes() },
                { Config.DividedBySign, new ParsedDividedBy() },
                { Config.PlusSign, new ParsedPlus() },
                { Config.MinusSign, new ParsedMinus() },

                { Config.EqualSign, new ParsedEqual() },
                { Config.NotEqualSign, new ParsedNotEqual() },
                { Config.GreaterOrEqualSign, new ParsedGreaterOrEqual() },
                { Config.GreaterThanSign, new ParsedGreaterThan() },
                { Config.LessOrEqualSign, new ParsedLessOrEqual() },
                { Config.LessThanSign, new ParsedLessThan() },

                { Config.AndSign, new ParsedAnd() },
                { Config.XorSign, new ParsedXor() },
                { Config.OrSign, new ParsedOr() },
                { Config.NegationSign, new ParsedNegation() },
            };
    }
}
