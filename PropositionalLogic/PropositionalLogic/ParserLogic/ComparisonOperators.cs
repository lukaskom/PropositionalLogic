﻿using PropositionalLogic.Operator.Comparison;

namespace PropositionalLogic.ParserLogic
{
    public class ParsedGreaterThan : ParsedItem
    {
        public override int Priority => 70;
        public override FormulaNode ToNode(Configuration config) => new GreaterThan(config);
    }

    public class ParsedGreaterOrEqual : ParsedItem
    {
        public override int Priority => 70;
        public override FormulaNode ToNode(Configuration config) => new GreaterOrEqual(config);
    }

    public class ParsedLessThan : ParsedItem
    {
        public override int Priority => 70;
        public override FormulaNode ToNode(Configuration config) => new LessThan(config);
    }

    public class ParsedLessOrEqual : ParsedItem
    {
        public override int Priority => 70;
        public override FormulaNode ToNode(Configuration config) => new LessOrEqual(config);
    }

    public class ParsedEqual : ParsedItem
    {
        public override int Priority => 70;
        public override FormulaNode ToNode(Configuration config) => new Equal(config);
    }

    public class ParsedNotEqual : ParsedItem
    {
        public override int Priority => 70;
        public override FormulaNode ToNode(Configuration config) => new NotEqual(config);
    }

}
