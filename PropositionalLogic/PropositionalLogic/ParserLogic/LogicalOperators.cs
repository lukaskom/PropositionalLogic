﻿using PropositionalLogic.Operator.Boolean;

namespace PropositionalLogic.ParserLogic
{
    public class ParsedAnd : ParsedItem
    {
        public override int Priority => 40;
        public override FormulaNode ToNode(Configuration config) => new And(config);
    }

    public class ParsedXor : ParsedItem
    {
        public override int Priority => 30;
        public override FormulaNode ToNode(Configuration config) => new Xor(config);
    }

    public class ParsedOr : ParsedItem
    {
        public override int Priority => 20;
        public override FormulaNode ToNode(Configuration config) => new Or(config);
    }

    public class ParsedNegation : ParsedItem
    {
        public override int Priority => 50;
        public override FormulaNode ToNode(Configuration config) => new Negation(config);
    }

}
