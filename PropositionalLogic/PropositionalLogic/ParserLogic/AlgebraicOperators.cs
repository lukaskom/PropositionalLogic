﻿using PropositionalLogic.Operator.Algebraic;

namespace PropositionalLogic.ParserLogic
{
    public class ParsedTimes : ParsedItem
    {
        public override int Priority => 100;
        public override FormulaNode ToNode(Configuration config) => new Times(config);
    }

    public class ParsedDividedBy : ParsedItem
    {
        public override int Priority => 100;
        public override FormulaNode ToNode(Configuration config) => new DividedBy(config);
    }

    public class ParsedPlus : ParsedItem
    {
        public override int Priority => 90;
        public override FormulaNode ToNode(Configuration config) => new Plus(config);
    }

    public class ParsedMinus : ParsedItem
    {
        public override int Priority => 90;
        public override FormulaNode ToNode(Configuration config) => new Minus(config);
    }

}
