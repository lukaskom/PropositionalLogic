﻿using PropositionalLogic.Operator;
using System.Collections.Generic;

namespace PropositionalLogic.ParserLogic
{
    public class ParsedParentheses : ParsedItem
    {
        public string StringContent { get; set; }

        public IList<ParsedItem> ParsedContent { get; set; } = new List<ParsedItem>();

        public override int Priority => 500;

        public ParsedParentheses(string stringContent)
        {
            StringContent = stringContent;
        }

        public ParsedParentheses(string stringContent, IList<ParsedItem> parsedContent)
        {
            StringContent = stringContent;
            ParsedContent = parsedContent;
        }

        public override FormulaNode ToNode(Configuration config) => new Parentheses(config);

    }
}
