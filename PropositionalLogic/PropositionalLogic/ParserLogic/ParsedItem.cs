﻿namespace PropositionalLogic.ParserLogic
{
    public abstract class ParsedItem
    {
        public bool WasProcessed = false;
        public abstract int Priority { get; }

        public abstract FormulaNode ToNode (Configuration config);

    }
}
