﻿using PropositionalLogic.Code;
using PropositionalLogic.Formula;

namespace PropositionalLogic.ParserLogic
{
    public class Unknown : ParsedItem
    {
        public string StringRepresentation { get; set; }

        public override int Priority => 1000;

        public override FormulaNode ToNode(Configuration config)
        {
            if (config.TryParseConstant(StringRepresentation, config, out var t))
                return new Constant(config, StringRepresentation) { Type = t.type, Value = t.value };

            if (config.TryParseVariable(StringRepresentation, config, out var type))
                return new Variable(config, StringRepresentation) { Type = type };

            throw new SolverException($"Invalid constant or variable name: {StringRepresentation}");
        }

        public Unknown(string stringRepresentation)
        {
            StringRepresentation = stringRepresentation;
        }

    }
}
