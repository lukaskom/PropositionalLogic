﻿namespace PropositionalLogic
{
    public abstract class FormulaNode
    {
        public FormulaNode(Configuration config) => Config = config;

        public Configuration Config { get; set; }

        public override abstract string ToString();

    }
}
