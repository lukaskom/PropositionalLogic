﻿namespace PropositionalLogic.Operator.Boolean
{
    public class Negation : UnaryOperator
    {
        public Negation(Configuration config) : base(config) { }

        public override string ToString() => $"{Config.NegationSign}{ChildNode}";

    }
}
