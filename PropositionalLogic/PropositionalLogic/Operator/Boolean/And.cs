﻿namespace PropositionalLogic.Operator.Boolean
{
    public class And : BinaryOperator
    {
        public And(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.AndSign} {RightChildNode}";

    }
}
