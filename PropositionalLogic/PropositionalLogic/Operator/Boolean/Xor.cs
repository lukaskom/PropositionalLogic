﻿namespace PropositionalLogic.Operator.Boolean
{
    public class Xor : BinaryOperator
    {
        public Xor(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.XorSign} {RightChildNode}";

    }
}
