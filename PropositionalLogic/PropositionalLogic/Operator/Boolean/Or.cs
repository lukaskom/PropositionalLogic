﻿namespace PropositionalLogic.Operator.Boolean
{
    public class Or : BinaryOperator
    {
        public Or(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.OrSign} {RightChildNode}";

    }
}
