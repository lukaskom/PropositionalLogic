﻿namespace PropositionalLogic.Operator
{
    public class Parentheses : UnaryOperator
    {
        public Parentheses(Configuration config) : base(config) { }

        public override string ToString() => $"{Config.OpenParenthesisSign}{ChildNode}{Config.ClosedParenthesisSign}";

    }
}
