﻿namespace PropositionalLogic.Operator
{
    public abstract class UnaryOperator : FormulaNode
    {
        public UnaryOperator(Configuration config) : base(config) { }

        public FormulaNode ChildNode { get; set; }

    }
}
