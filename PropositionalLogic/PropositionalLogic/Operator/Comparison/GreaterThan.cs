﻿namespace PropositionalLogic.Operator.Comparison
{
    public class GreaterThan : BinaryOperator
    {
        public GreaterThan(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.GreaterThanSign} {RightChildNode}";

    }
}
