﻿namespace PropositionalLogic.Operator.Comparison
{
    public class Equal : BinaryOperator
    {
        public Equal(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.EqualSign} {RightChildNode}";

    }
}
