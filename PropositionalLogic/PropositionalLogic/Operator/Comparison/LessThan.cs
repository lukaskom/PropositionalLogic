﻿namespace PropositionalLogic.Operator.Comparison
{
    public class LessThan : BinaryOperator
    {
        public LessThan(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.LessThanSign} {RightChildNode}";

    }
}
