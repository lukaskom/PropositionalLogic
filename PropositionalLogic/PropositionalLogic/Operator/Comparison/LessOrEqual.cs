﻿namespace PropositionalLogic.Operator.Comparison
{
    public class LessOrEqual : BinaryOperator
    {
        public LessOrEqual(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.LessOrEqualSign} {RightChildNode}";

    }
}
