﻿namespace PropositionalLogic.Operator.Comparison
{
    public class NotEqual : BinaryOperator
    {
        public NotEqual(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.NotEqualSign} {RightChildNode}";

    }
}
