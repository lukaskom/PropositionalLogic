﻿namespace PropositionalLogic.Operator.Comparison
{
    public class GreaterOrEqual : BinaryOperator
    {
        public GreaterOrEqual(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.GreaterOrEqualSign} {RightChildNode}";

    }
}
