﻿namespace PropositionalLogic.Operator
{
    public abstract class BinaryOperator : FormulaNode
    {
        public BinaryOperator(Configuration config) : base(config) { }

        public FormulaNode LeftChildNode { get; set; }
        public FormulaNode RightChildNode { get; set; }

    }
}
