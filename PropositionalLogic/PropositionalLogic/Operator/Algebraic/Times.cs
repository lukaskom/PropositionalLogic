﻿namespace PropositionalLogic.Operator.Algebraic
{
    public class Times : BinaryOperator
    {
        public Times(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.TimesSign} {RightChildNode}";

    }
}
