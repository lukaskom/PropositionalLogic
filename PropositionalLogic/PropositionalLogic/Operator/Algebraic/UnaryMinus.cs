﻿namespace PropositionalLogic.Operator.Algebraic
{
    public class UnaryMinus : UnaryOperator
    {
        public UnaryMinus(Configuration config) : base(config) { }

        public override string ToString() => $"{Config.MinusSign}{ChildNode}";

    }
}
