﻿namespace PropositionalLogic.Operator.Algebraic
{
    public class Plus : BinaryOperator
    {
        public Plus(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.PlusSign} {RightChildNode}";

    }
}
