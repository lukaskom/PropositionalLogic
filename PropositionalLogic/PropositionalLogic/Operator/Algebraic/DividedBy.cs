﻿namespace PropositionalLogic.Operator.Algebraic
{
    public class DividedBy : BinaryOperator
    {
        public DividedBy(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.DividedBySign} {RightChildNode}";

    }
}
