﻿namespace PropositionalLogic.Operator.Algebraic
{
    public class Minus : BinaryOperator
    {
        public Minus(Configuration config) : base(config) { }

        public override string ToString() => $"{LeftChildNode} {Config.MinusSign} {RightChildNode}";

    }
}
