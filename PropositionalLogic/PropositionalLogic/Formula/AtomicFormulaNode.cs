﻿using System;
using System.Linq;

namespace PropositionalLogic.Formula
{
    public abstract class AtomicFormula : FormulaNode
    {
        public AtomicFormula(Configuration config, string stringRepresentation) : base(config)
        {
            if (string.IsNullOrWhiteSpace(stringRepresentation) || stringRepresentation.Any(char.IsWhiteSpace))
                throw new ArgumentException("Argument cannot be null nor contain whitespace characters.");

            StringRepresentation = stringRepresentation;
        }

        public string StringRepresentation { get; }

        public Type Type { get; set; }

    }
}
