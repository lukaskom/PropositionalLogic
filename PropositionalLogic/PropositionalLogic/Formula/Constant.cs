﻿namespace PropositionalLogic.Formula
{
    public class Constant : AtomicFormula
    {
        public object Value { get; set; }

        public Constant(Configuration config, string stringRepresentation) : base (config, stringRepresentation) { }

        public override string ToString() => StringRepresentation;

    }
}
