﻿namespace PropositionalLogic.Formula
{
    public class Variable : AtomicFormula
    {
        public Variable(Configuration config, string variableName) : base(config, variableName) { }

        public override string ToString() => StringRepresentation;

    }
}
