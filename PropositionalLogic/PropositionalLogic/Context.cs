﻿namespace PropositionalLogic
{
    public class Context
    {
        public bool WasComparisonOperatorUsed { get; set; }

        public bool CanUseBooleanOperator => !WasComparisonOperatorUsed;
        public bool CanUseComparisonOperator => !WasComparisonOperatorUsed;

    }
}
