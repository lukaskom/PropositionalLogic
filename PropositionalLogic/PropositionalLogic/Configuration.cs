﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PropositionalLogic
{
    public class Configuration
    {
        public string TimesSign { get; set; } = "*";
        public string DividedBySign { get; set; } = "/";
        public string PlusSign { get; set; } = "+";
        public string MinusSign { get; set; } = "-";
        
        public string AndSign { get; set; } = "&&";
        public string XorSign { get; set; } = "^";
        public string OrSign { get; set; } = "||";
        public string NegationSign { get; set; } = "!";

        public string EqualSign { get; set; } = "==";
        public string NotEqualSign { get; set; } = "!=";
        public string GreaterOrEqualSign { get; set; } = ">=";
        public string GreaterThanSign { get; set; } = ">";
        public string LessOrEqualSign { get; set; } = "<=";
        public string LessThanSign { get; set; } = "<";

        public string OpenParenthesisSign { get; set; } = "(";
        public string ClosedParenthesisSign { get; set; } = ")";

        public Dictionary<string, Type> VariableNames { get; set; }
            = new Dictionary<string, Type>();

        public delegate bool TryParseConstantDelegate(string str, Configuration config,
            out (Type type, object value) t);
        public TryParseConstantDelegate TryParseConstant
            = delegate (string str, Configuration config, out (Type type, object value) t)
            {
                t = default((Type, object));
                if (int.TryParse(str, out int num))
                {
                    t = (typeof(int), num);
                    return true;
                }
                if (bool.TryParse(str, out bool b))
                {
                    t = (typeof(bool), b);
                    return true;
                }
                return false;
            };

        public delegate bool TryParseVariableDelegate(string str, Configuration config, out Type type);
        public TryParseVariableDelegate TryParseVariable
            = delegate (string str, Configuration config, out Type type)
            {
                if (config.VariableNames.Any())
                    return config.VariableNames.TryGetValue(str, out type);

                type = default(Type);

                if (!string.IsNullOrWhiteSpace(str)
                        && (char.IsLetter(str.First()) || str.First() == '_')
                        && str.All(t => t == '_' || char.IsLetterOrDigit(t)))
                    return true;

                return false;
            };

        public string[] GetAllSeparators
            => new string[]
            {
                TimesSign,
                DividedBySign,
                PlusSign,
                MinusSign,

                AndSign,
                XorSign,
                OrSign,
                NegationSign,

                EqualSign,
                NotEqualSign,
                GreaterOrEqualSign,
                GreaterThanSign,
                LessOrEqualSign,
                LessThanSign,

                OpenParenthesisSign,
                ClosedParenthesisSign
            };
    }
}
