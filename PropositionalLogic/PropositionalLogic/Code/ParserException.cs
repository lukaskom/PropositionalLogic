﻿using System;

namespace PropositionalLogic.Code
{
    public class ParserException : Exception
    {
        public int Position { get; set; }

        public ParserException(string message, int position) : base(message)
        {
            Position = position;
        }
    }
}
