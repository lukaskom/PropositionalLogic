﻿using System;

namespace PropositionalLogic.Code
{
    public class SolverException : Exception
    {
        public SolverException(string message) : base(message) { }

    }
}
