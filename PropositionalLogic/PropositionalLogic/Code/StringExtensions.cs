﻿using System.Collections.Generic;
using System.Linq;

namespace PropositionalLogic.Code
{
    public static class StringExtensions
    {
        public static List<(int occurence, string item)> IndicesOfAny(this string str, string[] items)
        {
            var ret = new List<(int occurence, string item)>();

            if (!items.Any())
                return ret;

            foreach (var item in items)
            {
                if (string.IsNullOrEmpty(item))
                    continue;

                var pos = str.IndexOf(item);
                while(pos >= 0)
                {
                    if (!ret.Any(t => (t.occurence <= pos && pos <= t.occurence + t.item.Length - 1)
                        || (t.occurence <= pos + item.Length - 1
                            && pos + item.Length - 1 <= t.occurence + t.item.Length - 1)))
                    {
                        ret.Add((pos, item));
                    }

                    if (pos + item.Length == str.Length)
                        pos = -1;
                    else
                        pos = str.IndexOf(item, pos + item.Length);
                }
            }

            return ret.OrderBy(t => t.occurence).ToList();
        }
    }
}
