﻿using System;

namespace PropositionalLogic.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var line = "";
            var solver = new Solver();

            while ((line = Console.ReadLine()).ToLower() != "q")
            {
                Console.WriteLine(solver.Solve(line));
            }
        }
    }
}
