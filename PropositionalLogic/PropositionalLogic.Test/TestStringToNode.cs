﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PropositionalLogic.Formula;
using PropositionalLogic.Operator;

namespace PropositionalLogic.Test
{
    [TestClass]
    public class TestStringToNode
    {
        [TestMethod]
        public void TestToNode()
        {
            (var root, var str) = Data.CreateSimpleTree();
            
            var solvedRoot = new Solver().Solve(str);

            AssertNodeEquals(root, solvedRoot);
        }

        public bool AssertNodeEquals(FormulaNode expected, FormulaNode actual)
        {
            Assert.AreEqual(expected.GetType(), actual.GetType());

            if ((expected is UnaryOperator unaryExpected) && (actual is UnaryOperator unaryActual))
            {
                return AssertNodeEquals(unaryExpected.ChildNode, unaryActual.ChildNode);
            }

            if ((expected is BinaryOperator binaryExpected) && (actual is BinaryOperator binaryActual))
            {
                return AssertNodeEquals(binaryExpected.LeftChildNode, binaryActual.LeftChildNode)
                    && AssertNodeEquals(binaryExpected.RightChildNode, binaryActual.RightChildNode);
            }

            if ((expected is AtomicFormula atomExpected) && (actual is AtomicFormula atomActual))
            {
                return atomExpected.StringRepresentation == atomActual.StringRepresentation;
            }

            return false;
        }
    }
}
