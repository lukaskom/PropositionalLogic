using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PropositionalLogic.Test
{
    [TestClass]
    public class TestNodeToString
    {
        [TestMethod]
        public void TestToString()
        {
            (var root, var str) = Data.CreateSimpleTree();
            Assert.AreEqual(str, root.ToString());
        }
    }
}
