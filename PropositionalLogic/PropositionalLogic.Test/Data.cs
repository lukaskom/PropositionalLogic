﻿using PropositionalLogic.Formula;
using PropositionalLogic.Operator;
using PropositionalLogic.Operator.Algebraic;
using PropositionalLogic.Operator.Boolean;
using PropositionalLogic.Operator.Comparison;

namespace PropositionalLogic.Test
{
    public class Data
    {
        public static (FormulaNode root, string expectedStr) CreateSimpleTree()
        {
            var config = new Configuration();

            var root = new Or(config)
            {
                LeftChildNode = new And(config)
                {
                    LeftChildNode = new GreaterThan(config)
                    {
                        LeftChildNode = new Times(config)
                        {
                            LeftChildNode = new Parentheses(config)
                            {
                                ChildNode = new Plus(config)
                                {
                                    LeftChildNode = new Variable(config, "F"),
                                    RightChildNode = new Constant(config, "2")
                                }
                            },
                            RightChildNode = new Variable(config, "A")
                        },
                        RightChildNode = new Constant(config, "0")
                    },
                    RightChildNode = new Variable(config, "B")
                },
                RightChildNode = new GreaterOrEqual(config)
                {
                    LeftChildNode = new Plus(config)
                    {
                        LeftChildNode = new Variable(config, "C"),
                        RightChildNode = new Constant(config, "1")
                    },
                    RightChildNode = new Variable(config, "D")
                }
            };

            return (root, "(F + 2) * A > 0 && B || C + 1 >= D");
        }
    }
}
