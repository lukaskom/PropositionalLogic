using Microsoft.VisualStudio.TestTools.UnitTesting;
using PropositionalLogic.Code;
using System.Collections.Generic;
using System.Linq;

namespace PropositionalLogic.Test
{
    [TestClass]
    public class TestStringExtensions
    {
        [TestMethod]
        public void TestIndexOfAny()
        {
            var arr = new string[]
            {
                "ah",
                "a",
                "b"
            };
            var list = "blaaah".IndicesOfAny(arr);
            var expected = new List<(int occurence, string item)>()
            {
                (0, "b"),
                (2, "a"),
                (3, "a"),
                (4, "ah"),
            };

            Assert.IsTrue(expected.SequenceEqual(list));
        }
    }
}
